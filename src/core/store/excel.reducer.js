import { defaultCellStyles } from "../../pages/excel-page/components/table/table.db";
import { 
    TABLE_RESIZE,
    TABLE_CHANGE_TEXT,
    TABLE_CELL_TEXT,
    HEADER_CHANGE,
    CELLS_STYLE_CHANGE
} from "./excel.actions";

export function excelReducer(state, action = {}) {
    const { type, payload } = action;

    switch(type) {

        case TABLE_RESIZE: {
            const { value, id, type } = payload;

            return {
                ...state,
                [type]: {
                    ...state[type],
                    [id]: value
                }
            }
        }

        case TABLE_CHANGE_TEXT: {
            const { text: formulaText, groupIds } = payload;
            const cellsTextState = Object.assign({}, 
                state.cellsTextState,
                groupIds
            )

            return {
                ...state,
                formulaText,
                cellsTextState
            }
        }

        case TABLE_CELL_TEXT: {
            const cellsTextState = Object.assign({}, 
                state.cellsTextState,
                payload
            );

            return {
                ...state,
                cellsTextState
            }
        }

        case HEADER_CHANGE: {
            const { value: headerText } = payload;
            
            return {
                ...state,
                headerText
            }
        }

        case CELLS_STYLE_CHANGE: {
            const { cellsStyleState: previousState } = state;
            const currentState = Object.entries(payload).reduce((accum, [id, styles]) => {
                const prev = previousState[id] || {};
                const curr = Object.assign({}, prev, styles);
                
                const css = Object.keys(curr)
                    .filter((prop) => defaultCellStyles[prop] !== curr[prop])
                    .reduce((accum, prop) => {
                        accum[prop] = curr[prop];

                        return accum;
                    }, {});

                if(Object.keys(css).length) {
                    accum[id] = css;
                } else {
                    delete previousState[id]
                }

                return accum;
            }, {});
            const cellsStyleState = Object.assign({}, previousState, currentState);

            return {
                ...state,
                cellsStyleState
            }
        }

        default: {
            return state;
        }
    }
}
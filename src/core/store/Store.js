import { Storage } from "../classes/Storage";

export class Store {

    constructor(reducer, initialState, stateName = '') {
        let state = reducer(initialState);
        let listeners = [];

        Storage.set(stateName, state);

        return { 
            subscribe(fn) {
                listeners.push(fn);

                return {
                    unsubscribe() {
                        listeners = listeners.filter(l => l !== fn);
                    }
                }
            },
            dispatch(action) {
                state = reducer(state, action);
                Storage.set(stateName, state);
                listeners.forEach(l => l(state));
            },
            getState() {
                return state;
            }
        }
    }
}
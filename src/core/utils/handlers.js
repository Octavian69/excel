export function capitalize(string = '') {
    return string[0].toLocaleUpperCase() + string.slice(1);
}

export function range(start, end) {
    if(start > end) {
        [start, end] = [end, start];
    }
    const length = end - start + 1;

    return Array.from({length}, (v, k) => start + k);
}

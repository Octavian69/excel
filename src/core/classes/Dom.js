class Dom {

    constructor(selector) {
        this.$el = typeof selector === 'string'
            ? document.querySelector(selector)
            : selector;
    }

    get text() {
        return this.$el.textContent.trim();
    }

    set text(value) {
        this.$el.textContent = value.trim();
    }

    scrollTo() {
        this.$el.scrollIntoView({ behavior: 'smooth' });
    }

    append(node) {
        if(node instanceof Dom) {
            this.$el.append(node.$el);
        } else {
            this.$el.appendChild(node);
        }

        return this;
    }

    html(template) {    
        if(typeof template === 'string') {
            this.$el.innerHTML = template;
            return this;
        }

        return this.$el.outerHTML.trim();
    }

    clear() {
        return this.html('');
    }

    on(event, handler) {
        this.$el.addEventListener(event, handler);

        return this;
    }

    of(event, handler) {
        this.$el.removeEventListener(event, handler);
    }

    closest(selector) {
        return $(this.$el.closest(selector));
    }

    getCoords() {
        return this.$el.getBoundingClientRect();
    }

    setStyle(prop, value) {
        this.$el.style[prop] = value;
    }

    getStyle(property) {
        return getComputedStyle(this.$el)[property];
    }

    css(props) {
        Object.entries(props).forEach(([key, value]) => {
            this.$el.style[key] = value;
        })
    }

    equal($candidate) {
        if($candidate instanceof Dom) {
            return this.$el === $candidate.$el;
        }
        
        return this.$el === $candidate;
    }

    isHasClass(className) {
        return this.$el.classList.contains(className);
    }

    setClass(className) {
        this.$el.className = className;
    }

    addClass(className) {
        this.$el.classList.add(className);
    }

    removeClass(className) {
        this.$el.classList.remove(className);
    }

    toggleClass(className) {
        this.$el.classList.toggle(className);
    }

    getAttribute(attr) {
        return this.$el?.dataset[attr];
    }

    cssReset(css) {
        Object.keys(css).forEach(prop => {
            this.$el.style[prop] = '';
        })
    }

    id(separator) {
        if(separator) {
            return this.$el.dataset.id
                .split(separator)
                .map(Number)
        }

        return this.$el.dataset.id;
    }

    getChild(selector) {
        const child$ = this.$el.querySelector(selector);
        return child$ ? $(child$) : null;
    }

    getAll(selector) {
        const childs$ = this.$el.querySelectorAll(selector);
        const elems$ = Array.from(childs$).map(el$ => $(el$));
        
        return elems$;
    }

    focus() {
        this.$el.focus();
        // this.$el.scrollIntoView({ behavior: 'smooth' });
    }

    blur() {
        this.$el.blur();
    }

    getDOMValue(prop) {
        return this.$el[prop]
    }
}

export function $(selector) {
    return new Dom(selector);
}

$.create = (tagName, classes = '') => {
    const $el = document.createElement(tagName);

    if(classes) $el.classList.add(classes);

    return new Dom($el);
}
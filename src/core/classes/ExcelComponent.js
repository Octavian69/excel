import { DomListener } from "./DomListener";

export class ExcelComponent extends DomListener {

    constructor($root, options = {}) {
        super($root, options)
        this.name = options.name || '';
    }

    init() {
        this.initDOMListeners();
        this.initObservers();
        this.initStore();
    }
    
    toHTML() {
        return ''
    }

    destroy() {
        this.unsubscribe();
        this.storeUnsubscribe();
        this.removeDOMListeners();
    }
}
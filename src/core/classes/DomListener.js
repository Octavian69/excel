import { capitalize } from "../utils/handlers";

 export class DomListener {
    constructor($root, { 
        store = {},
        listeners = [], 
        observers = [], 
        observer = null
     }) {
        if(!$root) {
            throw new Error('Component initialized without root');
        }

        this.$root = $root;
        this.listenHandlers = {};
        this.observer = observer;
        this.listeners = listeners;
        this.observers = observers;
        this.store = store;
        this.storeSub = null;
    }

    initDOMListeners() {
        this.listeners.forEach(eventName => {
            const methodName = getMethodName(eventName);
            const method = this[methodName].bind(this);

            if(!method) {
                const msg = `Method ${methodName} has not implemented on ${this.name} component!`;
                throw new Error(msg)
            }

            this.$root.on(eventName, method);
            this.addListener(eventName, method);
        })
    }

    removeDOMListeners() {
        Object.keys(this.listenHandlers).forEach(eventName => {
            const subs$ = this.listenHandlers[eventName];

            subs$.forEach(cb => this.$root.of(eventName, cb));
        })
    }

    addListener(eventName, method) {
        const subs$ = this.listenHandlers[eventName]
        if(subs$) subs$.push(method);
        else {
            this.listenHandlers[eventName] = [method];
        }
    }

    initObservers() {}

    initStore() {}

    subscribe(eventName, cb) {
        if(this.observer) {
            const sub = this.observer.subscribe(eventName, cb);
            this.observers.push(sub)
        }
    }

    unsubscribe() {
        this.observers.forEach(sub => sub.unsubscribe());
    }

    storeUnsubscribe() {
        if(this.storeSub) this.storeSub.unsubscribe();
    }

    dispatch(action) {
        this.store.dispatch(action);
    }

    on(cb) {
        this.storeSub = this.store.subscribe(cb);
    }

    next(eventName, value) {
        this.observer.next(eventName, value);
    }

    getState() {
        return this.store.getState();
    }
}

function getMethodName(eventName) {
    return 'on' + capitalize(eventName);
}
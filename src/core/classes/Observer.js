export class Observer {
    constructor() {
        this.listeners = {}
    }

    next(eventName, value) {
        const listeners = this.listeners[eventName];


        if(listeners) listeners.forEach(cb => cb(value));
    }

    subscribe(eventName, cb) {
        const listeners = this.listeners[eventName] || [];
        listeners.push(cb)
        this.listeners[eventName] = listeners;


        return {
            unsubscribe: () => {
                const idx = listeners.findIndex(ls => ls === cb);
                listeners.splice(idx, 1)
            }
        }
    }
}




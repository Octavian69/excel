import './scss/index.scss';
import { Excel } from './pages/excel-page/components/excel/Excel';
import { Header } from './pages/excel-page/components/header/Header';
import { Toolbar } from './pages/excel-page/components/toolbar/Toolbar';
import { Formula } from './pages/excel-page/components/formula/Formula';
import { Table } from './pages/excel-page/components/table/Table';
import { Store } from './core/store/Store';
import { excelReducer } from './core/store/excel.reducer';
import { ExcelState } from './core/store/excel.state';
import { Storage } from './core/classes/Storage';

const excel = new Excel('#root', {
    components: [
        Header,
        Toolbar,
        Formula,
        Table
    ],
    store: new Store(
        excelReducer, 
        Storage.get('excel') || ExcelState,
         'excel'
        )
});

excel.render();



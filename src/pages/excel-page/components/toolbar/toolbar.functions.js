import { $ } from "../../../../core/classes/Dom";
import { defaultToolbar, toolbarProperty } from "./toolbar.db";

export function toolbarStylesHandler(e) {
    const { target } = e;
    const elem = target.closest('.excel__toolbar-btn');

    if(!elem) return

    const $btn = $(elem);
    const [ key, value ] = this.getButtonValue($btn);
    const defaultValue = defaultToolbar[key];

    if(defaultValue) {
        const segment = this.$root.getAll(`[${toolbarProperty}^="${key}:"]`);
        const active$ = segment.find($el => $el.isHasClass('active'));
        
        if(active$.equal($btn)) return
        else {
            active$.removeClass('active');
            $btn.addClass('active');
        } 
    } else {
        const method = $btn.isHasClass('active') 
            ? 'removeClass' 
            : 'addClass';

        $btn[method]('active');
    }

    this.next('toolbar:action', { [key]: value });
}

export function toolbarChangeState($cell) {
    this.reset();
    const { cellsStyleState } = this.getState();
    const id = $cell.id();
    const styles = Object.assign(
        {},
        defaultToolbar,
        cellsStyleState[id]
    );

    this.$buttons.forEach($btn => {
        const [property, value] = this.getButtonValue($btn);

        if(styles[property] === value) $btn.addClass('active');
    })
}
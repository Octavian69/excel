export const toolbarActions = [
    { 
        icon: 'format_bold', 
        property: 'fontWeight:700'
    },
    { 
        icon: 'format_align_left',
        property: 'textAlign:left'
    },
    { 
        icon: 'format_align_center',
        property: 'textAlign:center'
    },
    { 
        icon: 'format_align_right',
        property: 'textAlign:right'
    },
    { 
        icon: 'format_italic',
        property: 'fontStyle:italic'
    },
    { 
        icon: 'format_underlined',
        property: 'textDecoration:underline'
    }
];

export const defaultToolbar = {
    textAlign: 'left'
}

export const toolbarProperty = 'data-toolbar-property';
import { toolbarActions, toolbarProperty } from "./toolbar.db";

export function createToolbar() {
    const template = toolbarActions.map(({ icon, property }) => {

        return  `
            <button 
                class="excel__toolbar-btn"
                ${toolbarProperty} = "${property}"
            >
                <i class="material-icons">${icon}</i>
            </button>
        `
    }).join('\n');

    return template;
}
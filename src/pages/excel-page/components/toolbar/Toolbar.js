import { ExcelComponent } from "../../../../core/classes/ExcelComponent";
import { createToolbar } from "./toolbar.template";
import { toolbarStylesHandler, toolbarChangeState } from "./toolbar.functions";

export class Toolbar extends ExcelComponent {
    static ClassName = 'excel__toolbar';

    constructor($root, options) {
        super($root, {
            listeners: ['click'],
            ...options
        });
        this.$buttons = [];
    }

    init() {
        super.init();
        this.$buttons = this.$root.getAll('.excel__toolbar-btn');
    }

    initObservers() {
        this.initTableSub();
    }

    initTableSub() {
        this.subscribe('table:select', toolbarChangeState.bind(this));
    }

    reset() {
        this.$buttons.forEach($btn => $btn.removeClass('active'));
    }

    toHTML() {
        return createToolbar();
    }

    getButtonValue($btn) {
        return $btn.getAttribute('toolbarProperty').split(':');
    }

    onClick(e) {
        toolbarStylesHandler.call(this, e);
    }
}
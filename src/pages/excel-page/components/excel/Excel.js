import { $ } from "../../../../core/classes/Dom";
import { Observer } from "../../../../core/classes/Observer";
import { ExcelComponent } from "../../../../core/classes/ExcelComponent";

export class Excel extends ExcelComponent {
    static ClassName = 'excel';

    constructor(selector, options = {}) {
        super($(selector), {
            observer: new Observer(),
            store: options.store
        });

        this.components = options.components || [];
    }

    getRoot() {
        const $root = $.create('div', Excel.ClassName);
        const  { store, observer } = this;
        const options = { store, observer };

        this.components = this.components.map(Component => {
            const $el = $.create('div', Component.ClassName);
            
            const instance = new Component($el, options);

            $el.html(instance.toHTML());
            $root.append($el);

            return instance;
        })

        return $root;
    }

    render() {
        this.$root.append(this.getRoot());
        this.components.forEach(c => c.init());
    }

    destroy() {
        this.components.forEach(c => c.destroy());
        this.storeUnsubscribe();
    }
}
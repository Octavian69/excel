import { ExcelComponent } from '@core/classes/ExcelComponent';

export class Formula extends ExcelComponent {
    static ClassName = 'excel__formula';

    constructor($root, options) {
        super($root, {
            name: 'Formula',
            listeners: ['input', 'focusout', 'keydown'],
            ...options
        });

        this.$input = null;
    }

    init() {
        super.init();
        this.$input = this.$root.getChild('.excel__formula-input');
    }

    toHTML() {
        const { cellsTextState } = this.store.getState();
        const template = cellsTextState['0:0'] || '';

        return `
            <div class="excel__formula-info">fx</div>
            <div 
                class="excel__formula-input" 
                contenteditable = 'true' 
                spellcheck="false">${template}</div>
        `
    }

    initStore() {
    }

    onInput(e) {
        // this.next('formula:done', this.$input.text);
    }

    onFocusout() {
        // this.removeDOMListeners();
    }

    initObservers() {
        this.subscribe('table:select', ($cell) => {
            this.$input.text = $cell.text;
        })

        this.subscribe('table:input', (value) => {
            this.$input.text = value;
        })
    }

    onKeydown(e) {
        const { key } = e;

        if(key === 'Enter') {
            e.preventDefault();
            this.next('formula:done', this.$input.text);
            this.$input.blur();
        }
    }
}
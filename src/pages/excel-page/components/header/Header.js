import { ExcelComponent } from "../../../../core/classes/ExcelComponent";
import { HEADER_CHANGE } from "../../../../core/store/excel.actions";

export class Header extends ExcelComponent {
    static ClassName = 'excel__header';

    constructor($root, options) {
        super($root, {
            listeners: ['input'],
            ...options
        });
    }

    toHTML() {
        const { headerText } = this.store.getState();

        return `
            <input type="text" class="input" value = '${headerText}'>
            <div class="actions">
                <button class="actions__btn">
                    <i class="material-icons">delete</i>
                </button>
                <button class="actions__btn">
                    <i class="material-icons">exit_to_app</i>
                </button>
            </div>
        `
    }

    onInput(e) {
        const { value } = e.target;
        
        this.dispatch({
            type: HEADER_CHANGE,
            payload: { value }
        })
    }
}
import { $ } from "../../../../core/classes/Dom";

export function tableResizer($el, $root) {

    return new Promise((resolve) => {
        const { resize: type, id } = $el.dataset;
        const $resizer = $($el);
        const $parent = $resizer.closest('[data-type="resizable"]');
        const coords = $parent.getCoords();
        const rootCoords = $root.getCoords();
        const activeClass = `active-resize-${type}`;
        const cursor = `${type}-resize`;
    
        let $info;
        let columnPosition;
        let cellSelector;
        let cells;
        let resizerCss = {};
        let resizeState = {
            id,
            type: type === 'col' ? 'colState' : 'rowState'
        }
    
        if(type === 'col') {
            columnPosition = $parent.getAttribute('columnPosition');
            cellSelector = `[data-cell-position="${columnPosition}"]`;
            cells = $root.getAll(cellSelector);
            resizerCss.position = 'fixed'
            resizerCss.top = rootCoords.top + 'px';
            resizerCss.height = rootCoords.height + 'px';
        } else {
            $info = $parent.getChild('[data-type="info"]');
            $parent.setStyle('position', 'relative');
            $info.setStyle('position', 'static');
        }
    
        document.ondrag = null;
        $resizer.addClass(activeClass);
        document.body.style.cursor = cursor;
    
        document.onmousemove = (event) => {
            const { prop, value }  = calculate(event, coords, type);
    
            if(type === 'col') {
                resizerCss.left = event.clientX - $resizer.getDOMValue('offsetWidth') + 'px';
                $resizer.css(resizerCss)
            } 
    
            $parent.setStyle(prop, value);
            resizeState.value = value;
        }
        
        document.onmouseup = _ => {
            if(type === 'col') {
                $resizer.cssReset(resizerCss);
                const width =  $parent.getDOMValue('offsetWidth') + 'px';
                cells.forEach(cell => {
                    cell.setStyle('width', width);
                })
            } else {
                $info.setStyle('position', 'relative');
                $parent.setStyle('position', '');
            }
    
            $resizer.removeClass(activeClass);
            document.onmousemove = null;
            document.body.style.cursor = '';

            resolve(resizeState)
        } 
    })
    
}

export function calculate(event, coords, type) {
    let prop;
    let value;

    switch(type) {

        case 'col': {
            const { right, width } = coords;
            const delta = event.clientX - right;
            value = delta + width + 'px';
            prop = 'width';
            break;
        }

        case 'row': {
            const { bottom, height } = coords;
            const delta = event.clientY - bottom;
            value = height + delta + 'px';
            prop = 'height';

            break;
        }
    }

    return { prop, value };
}
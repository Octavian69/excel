export const defaultCellStyles = {
    fontWeight: 'normal',
    textAlign: 'left',
    fontStyle: 'normal',
    textDecoration: 'none'
}
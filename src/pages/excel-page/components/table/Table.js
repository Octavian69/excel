import { ExcelComponent } from "../../../../core/classes/ExcelComponent";
import { createTable } from './table.template';
import { shouldResize, isCell, matrix, nextCellId } from "./table.functions";
import { tableResizer } from "./table-resizer";
import { TableSelection } from "./TableSelection";
import { $ } from '../../../../core/classes/Dom';
import { 
    TABLE_RESIZE, 
    TABLE_CHANGE_TEXT, 
    TABLE_CELL_TEXT, 
    CELLS_STYLE_CHANGE } from "../../../../core/store/excel.actions";
import { defaultCellStyles } from "./table.db";

export class Table extends ExcelComponent {
    static ClassName = 'excel__table';

    constructor($root, options) {
        super($root, {
            listeners: ['mousedown', 'keydown', 'input'],
            observers: [],
            ...options
        });

        this.keys = [
            'Enter',
            'Tab',
            'ArrowDown',
            'ArrowUp',
            'ArrowRight',
            'ArrowLeft'
        ];

        this.selection = null;
        this.prepare();
    }

    prepare() {
        this.selection = new TableSelection();
    }

    init() {
        super.init();
        this.initCell();
    }

    initObservers() {
        this.initFormulaSub();
        this.initToolbarSub();
    }

    initToolbarSub() {
        this.subscribe('toolbar:action', (options) => {
            const { group } = this.selection;
            const [ property ] = Object.keys(options);

            const payload = group.reduce((accum, current) => {
                
                const id = current.id();
                const currentValue = current.getStyle(property);

                let styles;

                if(!currentValue.includes(options[property])) {
                    styles = Object.assign({}, options);
                } else {
                    const defaultValue = defaultCellStyles[property];
                    styles = { [property]: defaultValue };
                }

                current.css(styles)
                accum[id] = styles;

                return accum;
            }, {});

            this.dispatch({ type: CELLS_STYLE_CHANGE, payload });
        })
    }

    initCell() {
        const $currentCell = this.$root.getChild('[data-id="0:0"]');
        const { cellsStyleState } = this.getState();
        const styles = cellsStyleState[$currentCell.id()];

        Object.entries(cellsStyleState).forEach(([id, styles]) => {
            const $cell = this.$root.getChild(`[data-id="${id}"]`);
            $cell.css(styles);
        })

        if(styles) {
            $currentCell.css(styles);
        }
        this.select($currentCell);
    }

    initFormulaSub() {
        this.subscribe('formula:done', (value) => {
            const { current, group } = this.selection;
            const groupIds = group.reduce((accum, cell) => {
                cell.text = value;

                accum[cell.id()] = value;

                return accum
            }, {});
            current.focus();
            current.scrollTo();

            this.store.dispatch({
                type: TABLE_CHANGE_TEXT,
                payload: {
                    formulaText: value,
                    groupIds
                }
            })
        });
    }

    toHTML() {
        return createTable();
    }

    resizeTable(e) {
       return tableResizer(e.target, this.$root);
    }

    async onMousedown(e) {
        if(shouldResize(e)) {
            const payload = await this.resizeTable(e);
            this.dispatch({ type: TABLE_RESIZE, payload });

        } else if(isCell(e)) {
            const $target = $(e.target);

            if(e.shiftKey) {
                const ids = matrix($target, this.selection.current);
                const group = ids.map(id => this.$root.getChild(`[data-id="${id}"]`));
                
                this.selection.selectGroup(group);
            } else {
                this.select($target);
                this.input($target.text);
            }
        }
    }

    onKeydown(e) {
        const { key, shiftKey } = e;

        if(this.keys.includes(key) && !shiftKey) {
            e.preventDefault();

            const { current } = this.selection;
            const nextId = nextCellId(current.id(":"), key);
            const $cell = this.$root.getChild(`[data-id="${nextId}"]`);

            if($cell) {
                current.blur();
                $cell.focus();
                this.select($cell);
                this.dispatch({type: 'TABLE SELECT'})
            }
        }
    }

    select($cell) {
        this.selection.select($cell);
        this.next('table:select', $cell);
    }

    input(text) {
        this.next('table:input', text);
    }

    onInput(e) {
        const { current } = this.selection;
        const id = current.id();
        const text = current.text;
        this.input(text);

        this.dispatch({
            type: TABLE_CELL_TEXT,
            payload: { [id]: e.target.textContent }
        })
    }
}

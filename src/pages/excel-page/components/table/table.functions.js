import { range } from "../../../../core/utils/handlers";

export function shouldResize(e) {
    const { target: $el } = e;

    if($el.dataset.resize) return true;
}

export function isCell(e) {
    const { target: $el } = e;

    if($el.dataset.type === "cell") return true;
}

export function matrix($target, $current) {
    
    const targetIds = $target.id(':');
    const currentIds = $current.id(':');

    const cols = range(currentIds[0], targetIds[0]);
    const rows = range(currentIds[1], targetIds[1]);

    return cols.reduce((accum, col) => {
        rows.forEach(row => accum.push(`${col}:${row}`));

        return accum;
    }, []);
}

export function nextCellId([col, row], key) {
    let id;

    switch(key) {
        case 'Enter':
        case 'ArrowDown': {
            id = [col, row + 1];
            break;
        }
            
        case 'Tab':
        case 'ArrowRight': {
            id = [col + 1, row];
            break;
        }
        case 'ArrowLeft': {
            id = [col - 1, row];
            break;
        }
        case 'ArrowUp': {
            id = [col, row - 1];
            break;
        }
    }

    return id.join(':');
}

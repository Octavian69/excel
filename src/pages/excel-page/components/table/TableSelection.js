export class TableSelection {
    static ClassName = 'selected';

    constructor() {
        this.group = [];
        this.current = null;
    }

    select($el) {
        this.clear();
        this.group.push($el);
        $el.addClass(TableSelection.ClassName);
        this.current = $el;
    }

    selectGroup(group) {
        this.clear();

        this.group = group;

        this.group.forEach($cell => $cell.addClass(TableSelection.ClassName));
    }

    clear() {
        this.group.forEach($c => $c.removeClass(TableSelection.ClassName));
        this.group = [];
    }
}
import { Storage } from "../../../../core/classes/Storage";

const CODES = {
    A: 65,
    Z: 90
}

function toColumn({ colState }) {
    return (template, idx) => `
        <div 
            class="column" 
            data-type="resizable" 
            data-column-position = "${idx + 1}"
            data-column-id = "${idx}"
            style="width:${colState[idx]}"
        >
            <div class="column-template">${template}</div>
            <div class = "column-resize" data-resize="col" data-id="${idx}"></div>
        </div>`
}

function toRow(tempalte, idx, { rowState }) {
    return `
        <div 
            class="row" 
            data-type="resizable"
            data-row-id = "${idx}"
            style="height:${rowState[idx]}"
        >
            <div class="row-info" data-type="info">
                <div class="row-info-text">${idx + 1}</div>
                <div class="row-resize" data-resize="row" data-id="${idx}"></div>
            </div>
            <div class="row-data">${tempalte}</div>
        </div>
    `;
}

function toCell(rowIdx, { colState, cellsTextState, cellsStyleState }) {
    
    return (template, colIdx) => {
        const id = `${colIdx}:${rowIdx}`
        const classes = Object.values(cellsStyleState[id] || {}).reduce((accum, className) => {
            return accum += ` ${className}`;
        }, 'cell')

        return `
            <div 
                class="${classes}"
                contenteditable = 'true' 
                spellcheck="false"
                data-cell-position = "${colIdx + 1}"
                data-type = "cell"
                style = "width: ${colState[colIdx]}"
                data-id = "${id}"
            >${cellsTextState[id] || ''}</div>
        `
    } 
}

function toChar(_, idx) {
    return String.fromCharCode(CODES.A + idx);
}

export function createTable() {
    const state = Storage.get('excel');
    const collsCount = CODES.Z - CODES.A + 1;
    const collsArray = new Array(collsCount).fill('');
    const headColls = collsArray
        .map(toChar)
        .map(toColumn(state))
        .join('');

    

    const thead = toRow(headColls, '№', state);
    const tbody = collsArray
    .map((_, rowIdx) => collsArray.map(toCell(rowIdx, state)).join(''))
    .map((coll, idx) => toRow(coll, idx, state))
    .join('');

    return thead + tbody;
}